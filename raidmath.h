/* Copyright (c) 2014 Cormac O'Brien
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef RAIDMATH_H
#define RAIDMATH_H

#include <math.h>
#define M_PI (3.14159265358979323846)
#include <stdio.h>

typedef struct vec3_t {
    float c[3];
} vec3_t;

typedef struct vec4_t {
    float c[4];
} vec4_t;

typedef struct mat4x4_t {
    vec4_t v[4];
} mat4x4_t;

typedef struct mat4x4uni_t {
    float c[4][4];
} mat4x4uni_t;

/* ________________________________________________________ vec3 declarations */
static inline vec3_t vec3(const float x, const float y, const float z);
static inline vec3_t vec3add(const vec3_t a, const vec3_t b);
static inline vec3_t vec3cross(const vec3_t a, const vec3_t b);
static inline float  vec3dot(const vec3_t a, const vec3_t b);
static inline float  vec3mag(const vec3_t v);
static inline vec3_t vec3norm(const vec3_t v);
static inline vec3_t vec3reflect(const vec3_t v, const vec3_t n);
static inline vec3_t vec3scale(const vec3_t v, const float s);
static inline vec3_t vec3sub(const vec3_t a, const vec3_t b);

/*_________________________________________________________ vec4 declarations */
static inline vec4_t vec4(const float x, const float y, const float z, const float w);
static inline vec4_t vec4add(const vec4_t a, const vec4_t b);
static inline vec4_t vec4cross(const vec4_t a, const vec4_t b);
static inline float  vec4dot(const vec4_t a, const vec4_t b);
static inline float  vec4mag(const vec4_t v);
static inline vec4_t vec4norm(const vec4_t v);
static inline vec4_t vec4reflect(const vec4_t v, const vec4_t n);
static inline vec4_t vec4scale(const vec4_t v, const float s);
static inline vec4_t vec4sub(const vec4_t a, const vec4_t b);

/* ______________________________________________________ mat4x4 declarations */
static inline mat4x4_t mat4x4(const vec4_t a, const vec4_t b, const vec4_t c, const vec4_t d);
static inline mat4x4_t mat4x4add(const mat4x4_t a, const mat4x4_t b);
static inline vec4_t   mat4x4col(const mat4x4_t m, const int c);
static inline mat4x4_t mat4x4id();
static inline mat4x4_t mat4x4mult(const mat4x4_t a, const mat4x4_t b);
static inline vec4_t   mat4x4mult_vec4(const mat4x4_t M, const vec4_t v);
static inline void     mat4x4print(const mat4x4_t m);
static inline mat4x4_t mat4x4rot(const mat4x4_t M, const vec3_t u, const float angle);
static inline vec4_t   mat4x4row(const mat4x4_t M, const int i);
static inline mat4x4_t mat4x4scale(const mat4x4_t a, const float k);
static inline mat4x4_t mat4x4scale_aniso(const mat4x4_t a, const float x, const float y, const float z);
static inline mat4x4_t mat4x4sub(const mat4x4_t a, const mat4x4_t b);
static inline mat4x4_t mat4x4translate(const mat4x4_t m, const float x, const float y, const float z);
static inline mat4x4_t mat4x4transpose(const mat4x4_t N);

static inline mat4x4uni_t mat4x4uni(const mat4x4_t m);

/* __________________________________________________________ vec3 operations */
static inline vec3_t vec3(const float x, const float y, const float z) {
    return (vec3_t) {{ x, y, z }};
}

static inline vec3_t vec3add(const vec3_t a, const vec3_t b) {
    return vec3(
        a.c[0] + b.c[0],
        a.c[1] + b.c[1],
        a.c[2] + b.c[2]
    );
}

static inline vec3_t vec3cross(const vec3_t a, const vec3_t b) {
    return vec3(
        a.c[1] * b.c[2] - a.c[2] * b.c[1],
        a.c[2] * b.c[0] - a.c[0] * b.c[2],
        a.c[0] * b.c[1] - a.c[1] * b.c[0]
    );
}

static inline float vec3dot(const vec3_t a, const vec3_t b) {
    return
        a.c[0] * b.c[0] +
        a.c[1] * b.c[1] +
        a.c[2] * b.c[2];
}

static inline float vec3mag(const vec3_t v) {
    return sqrtf(vec3dot(v, v));
}

static inline vec3_t vec3norm(const vec3_t v) {
    return vec3scale(v, 1.0f / vec3mag(v));
}

static inline void vec3print(const vec3_t v) {
    printf("[ ");
    for(int i = 0; i < 3; ++i) {
        printf("% f%s", v.c[i], i != 2 ? "," : "");
    }
    printf(" ]\n");
}   

static inline vec3_t vec3reflect(const vec3_t v, const vec3_t n) {
    const float p  = 2.0f * vec3dot(v, n);
    return vec3(
        v.c[0] - p * n.c[0],
        v.c[1] - p * n.c[1],
        v.c[2] - p * n.c[2]
    );
}

static inline vec3_t vec3scale(const vec3_t v, const float s) {
    return vec3(
        v.c[0] * s,
        v.c[1] * s,
        v.c[2] * s
    );
}

static inline vec3_t vec3sub(const vec3_t a, const vec3_t b) {
    return vec3(
        a.c[0] - b.c[0],
        a.c[1] - b.c[1],
        a.c[2] - b.c[2]
    );
}

/* __________________________________________________________ vec4_t operations */
static inline vec4_t vec4(const float x, const float y, const float z, const float w) {
    return (vec4_t) {{ x, y, z, w }};
}

static inline vec4_t vec4add(const vec4_t a, const vec4_t b) {
    return vec4(
        a.c[0] + b.c[0],
        a.c[1] + b.c[1],
        a.c[2] + b.c[2],
        a.c[3] + b.c[3]
    );
}

static inline vec4_t vec4cross(const vec4_t a, const vec4_t b) {
    return vec4(
        a.c[1] * b.c[2] - a.c[2] * b.c[1],
        a.c[2] * b.c[0] - a.c[0] * b.c[2],
        a.c[0] * b.c[1] - a.c[1] * b.c[0],
        1.0f
    );
}

static inline float vec4dot(const vec4_t a, const vec4_t b) {
    return
        a.c[0] * b.c[0] +
        a.c[1] * b.c[1] +
        a.c[2] * b.c[2] +
        a.c[3] * b.c[3];
}

static inline float vec4mag(const vec4_t v) {
    return sqrtf(vec4dot(v, v));
}

static inline vec4_t vec4norm(const vec4_t v) {
    return vec4scale(v, 1.0f / vec4mag(v));
}

static inline void vec4print(const vec4_t v) {
    printf("[ ");
    for(int i = 0; i < 4; ++i) {
        printf("% f%s", v.c[i], i != 3 ? "," : "");
    }
    printf(" ]\n");
}

static inline vec4_t vec4scale(const vec4_t v, const float s) {
    return vec4(
        v.c[0] * s,
        v.c[1] * s,
        v.c[2] * s,
        v.c[3] * s
    );
}

static inline vec4_t vec4sub(const vec4_t a, const vec4_t b) {
    return vec4(
        a.c[0] - b.c[0],
        a.c[1] - b.c[1],
        a.c[2] - b.c[2],
        a.c[3] - b.c[3]
    );
}

static inline vec4_t vec4reflect(const vec4_t v, const vec4_t n) {
    const float p  = 2.0f * vec4dot(v, n);
    return vec4(
        v.c[0] - p * n.c[0],
        v.c[1] - p * n.c[1],
        v.c[2] - p * n.c[2],
        v.c[3] - p * n.c[3]
    );
}

/* ________________________________________________________ mat4x4_t operations */
static inline mat4x4_t mat4x4(const vec4_t a, const vec4_t b, const vec4_t c, const vec4_t d) {
    return (mat4x4_t) {{ a, b, c, d }};
}

static inline mat4x4_t mat4x4add(const mat4x4_t a, const mat4x4_t b) {
    return mat4x4(
        vec4add(a.v[0], b.v[0]),
        vec4add(a.v[1], b.v[1]),
        vec4add(a.v[2], b.v[2]),
        vec4add(a.v[3], b.v[3])
    );
}

static inline vec4_t mat4x4col(const mat4x4_t M, const int c) {
    return M.v[c];
}

static inline mat4x4_t mat4x4id() {
    return mat4x4(
        vec4(1.f, 0.f, 0.f, 0.f),
        vec4(0.f, 1.f, 0.f, 0.f),
        vec4(0.f, 0.f, 1.f, 0.f),
        vec4(0.f, 0.f, 0.f, 1.f)
    );
}

static inline mat4x4_t mat4x4lookat(const vec3_t eye, const vec3_t center, const vec3_t up) {
    vec3_t f = vec3norm(vec3sub(center, eye));
    vec3_t s = vec3norm(vec3cross(f, up));
    vec3_t t = vec3cross(s, f);

    mat4x4_t look = mat4x4(
        vec4(s.c[0], t.c[0], -f.c[0], 0.0f),
        vec4(s.c[1], t.c[1], -f.c[1], 0.0f),
        vec4(s.c[2], t.c[2], -f.c[2], 0.0f),
        vec4(0.0f,   0.0f,    0.0f,   1.0f)
    );

    return mat4x4translate(look, -eye.c[0], -eye.c[1], -eye.c[2]);
}

static inline mat4x4_t mat4x4mult(const mat4x4_t a, const mat4x4_t b) {
    mat4x4_t M = mat4x4id();
    for(int v = 0; v < 4; ++v) {
        for(int c = 0; c < 4; ++c) {
            M.v[v].c[c] = 0.f;
            for(int k = 0; k < 4; ++k) {
                M.v[v].c[c] += a.v[k].c[c] * b.v[v].c[k];
            }
        }
    }
    return M;
}

static inline vec4_t mat4x4mult_vec4(const mat4x4_t M, const vec4_t v) {
    vec4_t r = vec4(0.f, 0.f, 0.f, 0.f);
    for(int i = 0; i < 4; ++i) {
        for(int j = 0; j < 4; ++j) {
            r.c[i] += M.v[j].c[i] * v.c[j];
        }
    }
    return r;
}

static inline mat4x4_t mat4x4perspective(const float y_fov, const float aspect, const float n, const float f) {
    const float a = 1.f / tan(y_fov / 2.f);
    return mat4x4(
        vec4(a/aspect, 0.0f, 0.0f,                0.0f),
        vec4(0.0f,     a,    0.0f,                0.0f),
        vec4(0.0f,     0.0f, -((f+n)/(f-n)),     -1.0f),
        vec4(0.0f,     0.0f, -((2.0f*f*n)/(f-n)), 0.0f)
    );
}

static inline void mat4x4print(const mat4x4_t m) {
    for(int i = 0; i < 4; ++i) {
        vec4print(m.v[i]);
    }
}

static inline mat4x4_t mat4x4rot(const mat4x4_t M, const vec3_t u, const float angle) {
    const float s = sinf(angle);
    const float c = cosf(angle);

    const float x = u.c[0];
    const float y = u.c[1];
    const float z = u.c[2];

    mat4x4_t rotmat = {{
        {{ x*x*(1-c) + c,   y*x*(1-c) - z*s, z*x*(1-c) + y*s, 0.0f }},
        {{ x*y*(1-c) + z*s, y*y*(1-c) + c,   z*y*(1-c) - x*s, 0.0f }},
        {{ x*z*(1-c) - y*s, y*z*(1-c) + x*s, z*z*(1-c) + c,   0.0f }},
        {{ 0.0f,            0.0f,            0.0f,            1.0f }}
    }};

    return mat4x4mult(M, rotmat);
}

static inline vec4_t mat4x4row(const mat4x4_t M, const int i) {
    return vec4(
        M.v[0].c[i],
        M.v[1].c[i],
        M.v[2].c[i],
        M.v[3].c[i]
    );
}

static inline mat4x4_t mat4x4scale(const mat4x4_t a, const float k) {
    return mat4x4(
        vec4scale(a.v[0], k),
        vec4scale(a.v[1], k),
        vec4scale(a.v[2], k),
        vec4scale(a.v[3], k)
    );
}

static inline mat4x4_t mat4x4scale_aniso(const mat4x4_t a, const float x, const float y, const float z) {
    return mat4x4(
        vec4scale(a.v[0], x),
        vec4scale(a.v[1], y),
        vec4scale(a.v[2], z),
        a.v[3]
    );
}

static inline mat4x4_t mat4x4sub(const mat4x4_t a, const mat4x4_t b) {
    return mat4x4(
        vec4sub(a.v[0], b.v[0]),
        vec4sub(a.v[1], b.v[1]),
        vec4sub(a.v[2], b.v[2]),
        vec4sub(a.v[3], b.v[3])
    );
}

static inline mat4x4_t mat4x4translate(const mat4x4_t m, const float x, const float y, const float z) {
    mat4x4_t translate = mat4x4(
        vec4(1.0f, 0.0f, 0.0f, x),
        vec4(0.0f, 1.0f, 0.0f, y),
        vec4(0.0f, 0.0f, 1.0f, z),
        vec4(0.0f, 0.0f, 0.0f, 1.0f)
    );

    return mat4x4mult(translate, m);
}

static inline mat4x4_t mat4x4transpose(const mat4x4_t N) {
    return mat4x4(
        vec4(N.v[0].c[0], N.v[1].c[0], N.v[2].c[0], N.v[3].c[0]),
        vec4(N.v[0].c[1], N.v[1].c[1], N.v[2].c[1], N.v[3].c[1]),
        vec4(N.v[0].c[2], N.v[1].c[2], N.v[2].c[2], N.v[3].c[2]),
        vec4(N.v[0].c[3], N.v[1].c[3], N.v[2].c[3], N.v[3].c[3])
    );
}

static inline mat4x4uni_t mat4x4uni(const mat4x4_t m) {
    return (mat4x4uni_t) {{
        { m.v[0].c[0], m.v[0].c[1], m.v[0].c[2], m.v[0].c[3] },
        { m.v[1].c[0], m.v[1].c[1], m.v[1].c[2], m.v[1].c[3] },
        { m.v[2].c[0], m.v[2].c[1], m.v[2].c[2], m.v[2].c[3] },
        { m.v[3].c[0], m.v[3].c[1], m.v[3].c[2], m.v[3].c[3] }
    }};
}

/* CHECKED UP TO HERE */

/*
static inline void mat4x4_invert(const mat4x4_t M) {
    const float s[] = {
        M[0][0]*M[1][1] - M[1][0]*M[0][1],
        M[0][0]*M[1][2] - M[1][0]*M[0][2],
        M[0][0]*M[1][3] - M[1][0]*M[0][3],
        M[0][1]*M[1][2] - M[1][1]*M[0][2],
        M[0][1]*M[1][3] - M[1][1]*M[0][3],
        M[0][2]*M[1][3] - M[1][2]*M[0][3]
    };

    const float c[] = {
        M[2][0]*M[3][1] - M[3][0]*M[2][1],
        M[2][0]*M[3][2] - M[3][0]*M[2][2],
        M[2][0]*M[3][3] - M[3][0]*M[2][3],
        M[2][1]*M[3][2] - M[3][1]*M[2][2],
        M[2][1]*M[3][3] - M[3][1]*M[2][3],
        M[2][2]*M[3][3] - M[3][2]*M[2][3] 
    };
    
    const float idet = 1.0f / ( s[0]*c[5]-s[1]*c[4]+s[2]*c[3]+s[3]*c[2]-s[4]*c[1]+s[5]*c[0] );

    mat4x4_t T;
    
    T[0][0] = ( M[1][1] * c[5] - M[1][2] * c[4] + M[1][3] * c[3]) * idet;
    T[0][1] = (-M[0][1] * c[5] + M[0][2] * c[4] - M[0][3] * c[3]) * idet;
    T[0][2] = ( M[3][1] * s[5] - M[3][2] * s[4] + M[3][3] * s[3]) * idet;
    T[0][3] = (-M[2][1] * s[5] + M[2][2] * s[4] - M[2][3] * s[3]) * idet;

    T[1][0] = (-M[1][0] * c[5] + M[1][2] * c[2] - M[1][3] * c[1]) * idet;
    T[1][1] = ( M[0][0] * c[5] - M[0][2] * c[2] + M[0][3] * c[1]) * idet;
    T[1][2] = (-M[3][0] * s[5] + M[3][2] * s[2] - M[3][3] * s[1]) * idet;
    T[1][3] = ( M[2][0] * s[5] - M[2][2] * s[2] + M[2][3] * s[1]) * idet;

    T[2][0] = ( M[1][0] * c[4] - M[1][1] * c[2] + M[1][3] * c[0]) * idet;
    T[2][1] = (-M[0][0] * c[4] + M[0][1] * c[2] - M[0][3] * c[0]) * idet;
    T[2][2] = ( M[3][0] * s[4] - M[3][1] * s[2] + M[3][3] * s[0]) * idet;
    T[2][3] = (-M[2][0] * s[4] + M[2][1] * s[2] - M[2][3] * s[0]) * idet;

    T[3][0] = (-M[1][0] * c[3] + M[1][1] * c[1] - M[1][2] * c[0]) * idet;
    T[3][1] = ( M[0][0] * c[3] - M[0][1] * c[1] + M[0][2] * c[0]) * idet;
    T[3][2] = (-M[3][0] * s[3] + M[3][1] * s[1] - M[3][2] * s[0]) * idet;
    T[3][3] = ( M[2][0] * s[3] - M[2][1] * s[1] + M[2][2] * s[0]) * idet;

    return T;
}

static inline mat4x4_t mat4x4_orthonormalize(const mat4x4_t M) {
    mat4x4_t R = M;
    
    R[2] = vec3norm(R[2]);
    R[1] = vec3sub(R[1], vec3scale(R[2], vec3dot(R[1], vec3norm(R[2]))));
    R[1] = vec3norm(vec3sub(R[1], vec3scale(R[2], vec3dot(R[1], vec3norm(R[2])))));
    
    R[0] = vec3norm(vec3sub(R[0], vec3scale(R[2], vec3dot(R[0], R[1]))));

    return R;
}

static inline mat4x4_t mat4x4_frustum(mat4x4_t M, const float l, const float r, const float b, const float t, const float n, const float f) {
    M[0][0] = 2.f*n/(r-l);
    M[0][1] = M[0][2] = M[0][3] = 0.f;
    
    M[1][1] = 2.*n/(t-b);
    M[1][0] = M[1][2] = M[1][3] = 0.f;

    M[2][0] = (r+l)/(r-l);
    M[2][1] = (t+b)/(t-b);
    M[2][2] = -(f+n)/(f-n);
    M[2][3] = -1.f;
    
    M[3][2] = -2.f*(f*n)/(f-n);
    M[3][0] = M[3][1] = M[3][3] = 0.f;
}

static inline mat4x4_t mat4x4_ortho(mat4x4_t M, const float l, const float r, const float b, const float t, const float n, const float f) {
    M[0][0] = 2.f/(r-l);
    M[0][1] = M[0][2] = M[0][3] = 0.f;

    M[1][1] = 2.f/(t-b);
    M[1][0] = M[1][2] = M[1][3] = 0.f;

    M[2][2] = -2.f/(f-n);
    M[2][0] = M[2][1] = M[2][3] = 0.f;
    
    M[3][0] = -(r+l)/(r-l);
    M[3][1] = -(t+b)/(t-b);
    M[3][2] = -(f+n)/(f-n);
    M[3][3] = 1.f;
}
*/

#endif
